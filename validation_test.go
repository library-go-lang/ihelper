package ihelper_test

import (
	"gitlab.com/library-go-lang/ihelper"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestIsEmailValidTrue(t *testing.T) {
	email := "aa@aa.com"

	isValid := ihelper.IsEmailValid(email)

	assert.Equal(t, true, isValid, "[TestIsEmailValidTrue] Should valid email")
}

func TestIsEmailValidFalse(t *testing.T) {
	email := "aaaa.com"

	isValid := ihelper.IsEmailValid(email)

	assert.Equal(t, false, isValid, "[TestIsEmailValidTrue] Should invalid email")
}

func TestIsPhoneValidTrue(t *testing.T) {
	phone := "819923"

	isValid := ihelper.IsPhoneValid(phone)

	assert.Equal(t, true, isValid, "[TestIsPhoneValidTrue] Should valid phone")
}

func TestIsPhoneValidFalse(t *testing.T) {
	phone := "aa"

	isValid := ihelper.IsPhoneValid(phone)

	assert.Equal(t, false, isValid, "[TestIsPhoneValidFalse] Should invalid phone")
}

func TestIsPhoneCodeValidTrue(t *testing.T) {
	phone := "62"

	isValid := ihelper.IsPhoneCodeValid(phone)

	assert.Equal(t, true, isValid, "[TestIsPhoneCodeValidTrue] Should valid phone code")
}

func TestIsPhoneCodeValidFalse(t *testing.T) {
	phone := "aa"

	isValid := ihelper.IsPhoneCodeValid(phone)

	assert.Equal(t, false, isValid, "[TestIsPhoneCodeValidTrue] Should invalid phone code")
}

func TestIsPhoneCodeInvalid(t *testing.T) {
	phone := "-1"

	isValid := ihelper.IsPhoneCodeValid(phone)

	assert.Equal(t, false, isValid, "[TestIsPhoneCodeValidTrue] Should invalid phone code")
}
