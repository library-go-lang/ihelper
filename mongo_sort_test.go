package ihelper_test

import (
	"gitlab.com/library-go-lang/ihelper"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
)

func TestToMongoSort(t *testing.T) {
	sorts := []string{
		"-last_name",
		"dob",
	}

	exp := bson.D{
		{Key: "last_name", Value: -1},
		{Key: "dob", Value: 1},
	}

	res := ihelper.ToMongoSort(sorts)

	assert.NotEmpty(t, res, "[TestToMongoSort] Should not empty")
	assert.Equal(t, exp, res, "[TestToMongoSort] Result should equal")
}

func TestToMongoSortEmpty(t *testing.T) {
	sorts := []string{}

	exp := bson.D{}

	res := ihelper.ToMongoSort(sorts)

	assert.Empty(t, res, "[TestToMongoSort] Should empty")
	assert.Equal(t, exp, res, "[TestToMongoSort] Result should equal")
}
