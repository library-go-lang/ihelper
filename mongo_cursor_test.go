package ihelper_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/library-go-lang/iencryption/mocks"
	"gitlab.com/library-go-lang/ihelper"
	"go.mongodb.org/mongo-driver/bson"
)

func TestToCursorString(t *testing.T) {
	mockRes := []byte{10}

	encrptMock := new(mocks.Encryptor)

	encrptMock.On("Encrypt", mock.Anything).
		Return(mockRes, nil)

	encrptMock.On("EncryptString", mock.Anything).
		Return("tes")

	mp := map[string]bson.M{}

	res, err := ihelper.ToCursorString(encrptMock, mp)

	assert.Equal(t, "tes", res, "[TestToCursorString] Result should \"tes\"")
	assert.NoError(t, err, "[TestToCursorString] Should not error")
}

func TestToCursorStringError(t *testing.T) {
	errx := fmt.Errorf("error test")

	encrptMock := new(mocks.Encryptor)

	encrptMock.On("Encrypt", mock.Anything).
		Return(nil, errx)

	mp := map[string]bson.M{}

	_, err := ihelper.ToCursorString(encrptMock, mp)

	assert.Error(t, err, "[TestToCursorStringError] Should error")
	assert.Equal(t, "error test", errx.Error(), "[TestToCursorStringError] Error should \"error test\"")
}
