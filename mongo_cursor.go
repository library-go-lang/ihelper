package ihelper

import (
	"encoding/json"

	appencryption "gitlab.com/library-go-lang/iencryption"
	"go.mongodb.org/mongo-driver/bson"
)

func ToCursorString(encryption appencryption.Encryptor, mapCursor map[string]bson.M) (lastCursor string, err error) {
	if mapCursor != nil {
		var lastCursorByte []byte
		toEncrypt, _ := json.Marshal(mapCursor)

		lastCursorByte, err = encryption.Encrypt(toEncrypt)
		if err != nil {
			return
		}

		lastCursor = encryption.EncryptString(lastCursorByte)
	}

	return
}
