package ihelper_test

import (
	"gitlab.com/library-go-lang/ihelper"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSearchArrayStringFoundLeft(t *testing.T) {
	ars := []string{"a", "b", "c"}
	s := "a"

	pos := ihelper.SearchInArrayString(ars, s)

	assert.Equal(t, 0, pos, "[TestSearchArrayStringFoundLeft] Position Should 0")
}

func TestSearchArrayStringFoundRight(t *testing.T) {
	ars := []string{"a", "b", "c"}
	s := "c"

	pos := ihelper.SearchInArrayString(ars, s)

	assert.Equal(t, 2, pos, "[TestSearchArrayStringFoundRight] Position Should 2")
}

func TestSearchArrayStringEmptyArray(t *testing.T) {
	ars := []string{}
	s := "c"

	pos := ihelper.SearchInArrayString(ars, s)

	assert.Equal(t, -1, pos, "[TestSearchArrayStringEmptyArray] Position Should -1")
}

func TestSearchArrayStringNotFound(t *testing.T) {
	ars := []string{"a", "b", "c"}
	s := "d"

	pos := ihelper.SearchInArrayString(ars, s)

	assert.Equal(t, -1, pos, "[TestSearchArrayStringFoundRight] Position Should -1")
}

func TestIsArrayStringExistFound(t *testing.T) {
	ars := []string{"a", "b", "c"}
	s := "a"

	isFound := ihelper.IsArrayStringExist(ars, s)

	assert.Equal(t, true, isFound, "[TestIsArrayStringExistFound] Should true")
}

func TestIsArrayStringExistNotFound(t *testing.T) {
	ars := []string{"a", "b", "c"}
	s := "d"

	isFound := ihelper.IsArrayStringExist(ars, s)

	assert.Equal(t, false, isFound, "[TestIsArrayStringExistNotFound] Should false")
}
