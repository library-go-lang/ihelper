package ihelper

import (
	"math/rand"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/oklog/ulid/v2"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func GenerateID(prefix ...string) (res string) {
	return GenerateUUID(prefix...)
}

func GenerateUUID(prefix ...string) (res string) {
	return strings.Join(prefix, "-") + uuid.NewString()
}

func GenerateObjectID(prefix ...string) (res string) {
	return strings.Join(prefix, "-") + primitive.NewObjectID().Hex()
}

func GenerateULID(prefix ...string) (res string) {
	entropy := ulid.Monotonic(rand.New(rand.NewSource(time.Now().UnixNano()+int64(GenerateRandom(0, 1000)))), 1000)
	res = strings.Join(prefix, "-") + ulid.MustNew(ulid.Timestamp(time.Now()), entropy).String()
	return
}

func GenerateTranscationID(generateAt time.Time, email, username, phone string, charlen int) (res string) {
	letters := "1217CHELS323412eafoo"

	email = GetAlphanumeric(email)
	if len(email) >= 3 {
		res += GenerateRandomString(email, 3)
	}

	username = GetAlphanumeric(username)
	if len(username) >= 3 {
		res += GenerateRandomString(username, 3)
	}

	if len(phone) >= 2 {
		res += GenerateRandomString(phone, 2)
	}

	res += GenerateRandomString(generateAt.Format("20060102150405999"), 6)
	res += GenerateRandomString(letters, charlen)

	return
}

func GenerateBookingCode(orderAt time.Time, email, username, phone string, charlen int) (res string) {
	letters := "1217CHELS323412eafoo"

	email = GetAlphanumeric(email)
	if len(email) >= 3 {
		res += GenerateRandomString(email, 3)
	}

	username = GetAlphanumeric(username)
	if len(username) >= 3 {
		res += GenerateRandomString(username, 3)
	}

	if len(phone) >= 2 {
		res += GenerateRandomString(phone, 2)
	}

	res += GenerateRandomString(ToTimeString(orderAt), 2)
	res += GenerateRandomString(letters, charlen)
	res += GenerateRandomString(ToTimeString(orderAt), 2)

	return
}

func GenerateSessionID(email, username, phone string) (res string) {
	res = time.Now().Format("20060102150405999") + "_" + GenerateID() + "_" + GenerateBookingCode(time.Now(), email, username, phone, 8)
	return
}

func GenerateProductCode(names ...string) (res string) {
	return GenerateProductCodeWithCode(true, names...)
}

func GenerateProductCodeWithCode(code bool, names ...string) (res string) {
	for _, name := range names {
		name = strings.TrimSpace(name)
		res += strings.ReplaceAll(name, " ", "-")
		res += "-"
	}

	if code {
		res += GenerateRandomString("1217CHELS323412eafoo", 6)
	}
	return
}
