package ihelper

import (
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
)

func RegisterPostEndpoint(logger *logrus.Logger, r chi.Router, handler http.HandlerFunc, pattern ...string) {
	path := strings.Join(pattern, "/")
	r.Post(path, handler)

	logger.Debug("REGISTERING [POST] | ", path)
}

func RegisterGetEndpoint(logger *logrus.Logger, r chi.Router, handler http.HandlerFunc, pattern ...string) {
	path := strings.Join(pattern, "/")
	r.Get(path, handler)

	logger.Debug("REGISTERING [GET]  | ", path)
}

func RegisterPutEndpoint(logger *logrus.Logger, r chi.Router, handler http.HandlerFunc, pattern ...string) {
	path := strings.Join(pattern, "/")
	r.Put(path, handler)

	logger.Debug("REGISTERING [PUT]  | ", path)
}

func RegisterDeleteEndpoint(logger *logrus.Logger, r chi.Router, handler http.HandlerFunc, pattern ...string) {
	path := strings.Join(pattern, "/")
	r.Delete(path, handler)

	logger.Debug("REGISTERING [DEL]  | ", path)
}

func RegisterPatchEndpoint(logger *logrus.Logger, r chi.Router, handler http.HandlerFunc, pattern ...string) {
	path := strings.Join(pattern, "/")
	r.Patch(path, handler)

	logger.Debug("REGISTERING [PATCH]  | ", path)
}
