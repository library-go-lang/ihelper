package ihelper_test

import (
	"gitlab.com/library-go-lang/ihelper"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadTimeLocationValid(t *testing.T) {
	loc := ihelper.LoadTimeLocation("Asia/Jakarta")

	assert.NotNil(t, loc, "[TestLoadTimeLocationValid] Location should not nil")
	assert.Equal(t, ihelper.TimeLocationJakarta, loc, "[TestLoadTimeLocationValid] Location should jakarta")
}

func TestLoadTimeLocationInvalid(t *testing.T) {
	loc := ihelper.LoadTimeLocation("Asia/Hanoi")

	assert.NotNil(t, loc, "[TestLoadTimeLocationInvalid] Location should not nil")
	assert.Equal(t, ihelper.TimeLocationUTC, loc, "[TestLoadTimeLocationInvalid] Location should utc / default")
}
