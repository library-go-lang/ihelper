package ihelper_test

import (
	"gitlab.com/library-go-lang/ihelper"
	"net/http"
	"testing"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
)

func TestRegisterPostEndpoint(t *testing.T) {
	ihelper.RegisterPostEndpoint(
		logrus.New(),
		chi.NewRouter(),
		func(rw http.ResponseWriter, r *http.Request) {},
		"/tes",
	)
}

func TestRegisterGetEndpoint(t *testing.T) {
	ihelper.RegisterGetEndpoint(
		logrus.New(),
		chi.NewRouter(),
		func(rw http.ResponseWriter, r *http.Request) {},
		"/tes",
	)
}

func TestRegisterPutEndpoint(t *testing.T) {
	ihelper.RegisterPutEndpoint(
		logrus.New(),
		chi.NewRouter(),
		func(rw http.ResponseWriter, r *http.Request) {},
		"/tes",
	)
}

func TestRegisterDeleteEndpoint(t *testing.T) {
	ihelper.RegisterDeleteEndpoint(
		logrus.New(),
		chi.NewRouter(),
		func(rw http.ResponseWriter, r *http.Request) {},
		"/tes",
	)
}
