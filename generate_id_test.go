package ihelper_test

import (
	"gitlab.com/library-go-lang/ihelper"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestGenerateProductCode(t *testing.T) {
	res := ihelper.GenerateProductCode("test", "id")

	assert.NotEmpty(t, res, "[TestGenerateProductCode] Result should not empty")
}

func TestGenerateSessionID(t *testing.T) {
	res := ihelper.GenerateSessionID("test@aa.com", "test", "test")

	assert.NotEmpty(t, res, "[TestGenerateSessionID] Result should not empty")
}

func TestGenerateObjectID(t *testing.T) {
	res := ihelper.GenerateObjectID("test@aa.com", "test", "test")

	assert.NotEmpty(t, res, "[TestGenerateObjectID] Result should not empty")
}

func TestGenerateULID(t *testing.T) {
	res := ihelper.GenerateULID("test@aa.com", "test", "test")

	assert.NotEmpty(t, res, "[TestGenerateULID] Result should not empty")
}

func TestGenerateTranscationID(t *testing.T) {
	res := ihelper.GenerateTranscationID(time.Now(), "test@aa.com", "test", "test", 6)

	assert.NotEmpty(t, res, "[TestGenerateTranscationID] Result should not empty")
}
