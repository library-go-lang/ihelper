package ihelper

import (
	"strconv"
	"time"
)

func ToTimeIntWithLocation(t time.Time, loc *time.Location) int64 {
	return ToTimeInt(MustLoadTimeWithLocation(t, loc))
}

func ToTimeInt(t time.Time) int64 {
	return t.UnixNano()
}

func ToTimeStringWithLocation(t time.Time, loc *time.Location) string {
	return ToTimeString(MustLoadTimeWithLocation(t, loc))
}

func ToTimeString(t time.Time) string {
	unix := ToTimeInt(t)
	return strconv.Itoa(int(unix))
}

func MustLoadTimeWithLocation(t time.Time, loc *time.Location) time.Time {
	if loc == nil {
		loc = TimeLocationUTC
	}

	return t.In(loc)
}

func MustParseTime(from, layout string) (res time.Time) {
	res, _ = time.Parse(layout, from)
	return
}

func MustParseTimeWithLocation(from, layout string, loc *time.Location) time.Time {
	t := MustParseTime(from, layout)
	return MustLoadTimeWithLocation(t, loc)
}
