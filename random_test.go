package ihelper_test

import (
	"gitlab.com/library-go-lang/ihelper"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGenerateRandom(t *testing.T) {
	res := ihelper.GenerateRandom(1, 1)

	exp := 1

	assert.Equal(t, exp, res, "[TestGenerateRandom] Result should same")
	assert.NotEmpty(t, res, "[TestGenerateRandom] Result should not empty")
}

func TestGenerateRandomString(t *testing.T) {
	res := ihelper.GenerateRandomString("aaaaa", 3)

	exp := "aaa"

	assert.Equal(t, exp, res, "[TestGenerateRandomString] Result should same")
	assert.NotEmpty(t, res, "[TestGenerateRandomString] Result should not empty")
}
