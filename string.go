package ihelper

import "regexp"

func StringOr(ss ...string) string {
	for _, s := range ss {
		if s != "" {
			return s
		}
	}

	return ""
}

func GetAlphanumeric(s string) (res string) {
	reg, _ := regexp.Compile("[^a-zA-Z0-9]+")

	res = reg.ReplaceAllString(s, "")
	return
}
