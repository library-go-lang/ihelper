package ihelper

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

// QueryStringToString definition
func QueryString(r *http.Request, key string) string {
	return strings.TrimSpace(r.URL.Query().Get(key))
}

func QueryStrings(r *http.Request, key string) []string {
	return r.URL.Query()[key]
}

func QueryStringToInts(r *http.Request, key string, def int) (res []int) {
	var st []string = QueryStrings(r, key)

	res = make([]int, len(st))
	for rr, s := range st {
		res[rr] = ToInt(s, def)
	}

	return
}

// QueryStringToInt definition
func QueryStringToInt(r *http.Request, key string, def int) int {
	s := QueryString(r, key)

	return ToInt(s, def)
}

// QueryStringToInt definition
func QueryStringToInt64(r *http.Request, key string, def int64) int64 {
	s := QueryString(r, key)

	return ToInt64(s, def)
}

// QueryStringToFloat64 definition
func QueryStringToFloat64(r *http.Request, key string, def float64) float64 {
	s := QueryString(r, key)

	return ToFloat64(s, def)
}

// QueryStringToFloat64 definition
func QueryStringToFloat64Ptr(r *http.Request, key string) *float64 {
	s := QueryString(r, key)
	if s == "" {
		return nil
	}

	v := ToFloat64(s, -1)
	return &v
}

// QueryStringToFloat64 definition
func QueryStringToBool(r *http.Request, key string) bool {
	s := QueryString(r, key)
	if s == "" {
		return false
	}

	v, err := strconv.ParseBool(s)
	if err != nil {
		return false
	}

	return v
}

// QueryStringToFloat64 definition
func QueryStringToPointerBool(r *http.Request, key string) *bool {
	s := QueryString(r, key)
	if s == "" {
		return nil
	}

	v, err := strconv.ParseBool(s)
	if err != nil {
		return nil
	}

	return &v
}

func QueryStringToTime(r *http.Request, key string) time.Time {
	return QueryStringToTimeWithFromat(r, key, time.RFC3339)
}

func QueryStringToTimeWithFromat(r *http.Request, key, format string) (t time.Time) {
	dte := QueryString(r, key)

	t = MustParseTime(dte, format)
	return
}
