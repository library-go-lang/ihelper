package ihelper_test

import (
	"gitlab.com/library-go-lang/ihelper"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStringOrFound(t *testing.T) {
	s := ihelper.StringOr("aa", "")

	assert.Equal(t, "aa", s, "[TestStringOrFound] Result Should \"aa\"")
}

func TestStringOrNotFound(t *testing.T) {
	s := ihelper.StringOr("", "")

	assert.Equal(t, "", s, "[TestStringOrNotFound] Result Should \"\"")
	assert.Empty(t, s, "[TestStringOrNotFound] Result should empty")
}

func TestGetAlphanumeric(t *testing.T) {
	s := ihelper.GetAlphanumeric("aa0@gmail.com")

	assert.Equal(t, "aa0gmailcom", s, "[TestGetAlphanumeric] Result Should \"aagmailcom\"")
}
