package ihelper

import (
	"sort"
)

func MinInt64(s ...int64) (r int64) {
	if len(s) == 0 {
		return
	}

	sort.Slice(s, func(i, j int) bool {
		return s[i] < s[j]
	})

	r = s[0]
	return
}

func MaxInt64(s ...int64) (r int64) {
	if len(s) == 0 {
		return
	}

	sort.Slice(s, func(i, j int) bool {
		return s[i] > s[j]
	})

	r = s[0]
	return
}

func MinInt(s ...int) (r int) {
	if len(s) == 0 {
		return
	}

	sort.Ints(s)

	r = s[0]
	return
}

func MaxInt(s ...int) (r int) {
	if len(s) == 0 {
		return
	}

	sort.Ints(s)

	r = s[len(s)-1]
	return
}

func MinFloat64(s ...float64) (r float64) {
	if len(s) == 0 {
		return
	}

	sort.Float64s(s)

	r = s[0]
	return
}

func MaxFloat64(s ...float64) (r float64) {
	if len(s) == 0 {
		return
	}

	sort.Float64s(s)

	r = s[len(s)-1]
	return
}
