package ihelper

import (
	"net/http"

	"github.com/go-chi/chi"
)

func ChiURLParamToString(r *http.Request, key string) string {
	return chi.URLParam(r, key)
}

func ChiURLParamToInt(r *http.Request, key string, def int) (res int) {
	id := ChiURLParamToString(r, key)

	res = ToInt(id, def)
	return
}

func ChiURLParamToInt64(r *http.Request, key string, def int64) (res int64) {
	id := ChiURLParamToString(r, key)

	res = ToInt64(id, def)
	return
}

func ChiURLParamToFloat64(r *http.Request, key string, def float64) (res float64) {
	id := ChiURLParamToString(r, key)

	res = ToFloat64(id, def)
	return
}

func ChiURLParamToFloat32(r *http.Request, key string, def float32) (res float32) {
	id := ChiURLParamToString(r, key)

	res = ToFloat32(id, def)
	return
}
