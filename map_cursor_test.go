package ihelper_test

import (
	"gitlab.com/library-go-lang/ihelper"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
)

func TestToMapCursor(t *testing.T) {
	res := ihelper.ToMapCursor(map[string]bson.M{
		"created_at": {
			"$lte": "2006-01-02T15:04:05Z",
		},
	})

	exp := bson.M{
		"created_at": bson.M{
			"$lte": time.Date(2006, time.January, 02, 15, 04, 05, 0, time.UTC),
		},
	}

	assert.NotEmpty(t, res, "[TestToMapCursor] Should not empty")
	assert.Equal(t, exp, res, "[TestToMapCursor] Result should same")
}

func TestToMapCursorString(t *testing.T) {
	res := ihelper.ToMapCursor(map[string]bson.M{
		"name": {
			"$eq": "test",
		},
	})

	exp := bson.M{
		"name": bson.M{
			"$eq": "test",
		},
	}

	assert.NotEmpty(t, res, "[TestToMapCursorString] Should not empty")
	assert.Equal(t, exp, res, "[TestToMapCursorString] Result should same")
}

func TestToMapCursorDefault(t *testing.T) {
	res := ihelper.ToMapCursor(map[string]bson.M{
		"age": {
			"$gte": 25,
		},
	})

	exp := bson.M{
		"age": bson.M{
			"$gte": 25,
		},
	}

	assert.NotEmpty(t, res, "[TestToMapCursorDefault] Should not empty")
	assert.Equal(t, exp, res, "[TestToMapCursorDefault] Result should same")
}
